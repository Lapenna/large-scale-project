package it.dao;

import it.common.interfa.Client;
import it.common.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class CRUDGenericManagerTest {
    String name,surname,type,address,city,state,email,user,pass;
    boolean isCustomer;
    float price;

    @Before
    public void setUp() throws Exception {
        name = "pippo";
        surname = "pluto";
        type = "test";
        address="via test";
        city = "Pisa";
        state = "Italy";
        email = "test@test.it";
        user="test789";
        pass="password11";
        isCustomer=false;
    }

    @Test
    public void create() {

        Login login;
        login = new it.common.model.Login();
        login.setUserLogin(user);
        login.setPasswordLogin(pass);
        login.setCustomer(isCustomer);
        login = (it.common.model.Login)CRUDGenericManager.create(login);

        Assert.assertTrue(login!=null);

        Customer customer = new Customer(login.getCustId(),name,surname,city,state,address,email);
        Client client = (Client)CRUDGenericManager.create(customer);
        Assert.assertTrue(client!=null);

        Dealer dealer = new Dealer(login.getCustId(),name,surname,city,state,address,email);
        client = (Client)CRUDGenericManager.create(dealer);
        Assert.assertTrue(client!=null);

        Shop shop = new Shop();
        shop.setName(name);
        shop.setShopType("pippo");
        shop.setCity(city);
        shop.setState(state);
        shop.setAddress(address);
        shop.setCustId(dealer.getCustId());
        shop = (Shop)CRUDGenericManager.create(shop);

        Assert.assertTrue(shop!=null);

        Service service = new Service();
        service.setPrice(price);
        service.setService(name);
        service.setShopId(shop.getShopId());

        service = (Service)CRUDGenericManager.create(service);

        Assert.assertTrue(service!=null);

        Transaction transaction = new Transaction();
        transaction.setPrice(price);
        transaction.setShopId(shop.getShopId());
        transaction.setCustId(customer.getCustId());
        transaction.setServiceId(service.getServiceId());
        //transaction.setService(service.getService());

        transaction = (Transaction)CRUDGenericManager.create(transaction);

        Assert.assertTrue(transaction !=null);

    }

    @Test
    public void update() {
    }

    @Test
    public void read() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void readAllShopById() {
    }
}