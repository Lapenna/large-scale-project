package it.dao.jpa;

import it.common.interfa.Client;
import it.common.model.Customer;
import it.dao.CRUDGenericManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.util.Date;

public class CustomerManagerTest {
    String firstName;
    String lastName;
    String city;
    String state;
    String address;
    String email;
    Date birthday;
    String custId;
    String updateName;

    @Before
    public void setUp() throws Exception {
        firstName = "Pippo";
        lastName = "Pluto";
        city = "Ozieri";
        state = "Italy";
        address = "via pluto";
        email = "pluto@gmail.com";
        birthday = new Date();
        custId="ff8080816e142d9c016e142da0c20000";

        updateName="paperino";
    }

    @Test
    public void TestCreate() {
        Customer customer = new Customer(firstName,lastName,city,state,address,email);
        Client client = (Client)CRUDGenericManager.create(customer);
        Assert.assertTrue(client!=null);
    }

    @Test
    public void read() {
        CustomerManager customerManager = new CustomerManager();

        Client client = customerManager.read(custId);

        Assert.assertTrue(client!=null);

    }

    @Test
    public void update() {
    /*    CustomerManager customerManager = new CustomerManager();
        customerManager.setup();
        Client client=customerManager.update(custId,updateName,lastName,city,state,address,email,birthday);
        customerManager.exit();
        Assert.assertTrue(client!=null);*/
    }

    @Test
    public void delete() {
        Customer customer = null;
        CustomerManager customerManager = new CustomerManager();

        boolean ok=customerManager.delete(customer);


        Assert.assertTrue(ok);
    }
}