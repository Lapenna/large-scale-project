package it.dao;

import it.common.interfa.Client;
import it.common.model.*;
import it.common.model.Transaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Date;

public class Dao {

    private static final Logger LOGGER = LogManager.getLogger(Dao.class);

    private String connectionString;
    private String dbUser;
    private String dbPwd;

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPwd() {
        return dbPwd;
    }

    public void setDbPwd(String dbPwd) {
        this.dbPwd = dbPwd;
    }

/*    public Customer getCustomer(String custId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Customer customer = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "SELECT * FROM customers where custId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, custId);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                customer = new Customer(resultSet.getString("custId"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("email"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("address"),
                        resultSet.getDate("birthday"));
            } else {
                LOGGER.info("Non esistono clienti con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return customer;
    }

    public Transaction getCustomerByCustId(String custId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Transaction transaction = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "SELECT * FROM customerToShop where custId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, custId);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                transaction = new Transaction(resultSet.getString("custId"),
                        resultSet.getString("shopId"),
                        resultSet.getString("transId"),
                        resultSet.getString("service"),
                        resultSet.getFloat("price"));

            } else {
                LOGGER.info("Non esistono transazioni con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return transaction;
    }

    public Shop getShop(String dealerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Shop shop = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());*/

/*
            final String QUERY = "SELECT * FROM shop where dealerId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, dealerId);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                shop = new Shop(resultSet.getString("shopId"),
                        resultSet.getString("dealerId"),
                        resultSet.getString("shopType"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("address"));


            } else {
                LOGGER.info("Non esistono attività commerciali con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return shop;
    }

    *//*public Service getService(String shopId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Service service = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "SELECT * FROM service where shopId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, shopId);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                service = new Service(resultSet.getString("service"),
                        resultSet.getString("shopId"),
                        resultSet.getFloat("price"),
                        resultSet.getInt("seq"));


            } else {
                LOGGER.info("Non esistono servizi con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return service;

    }*//*

    public Dealer getDealer(String dealerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Dealer dealer = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "SELECT * FROM dealer where dealerId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, dealerId);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                dealer = new Dealer(resultSet.getString("dealerId"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("email"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("address"),
                        resultSet.getDate("birthday"));
            } else {
                LOGGER.info("Non esistono clienti con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return dealer;
    }

    public boolean deleteCustomer(String custId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM customers where custId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, custId);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public boolean deleteDealer(String dealerId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM dealer where dealerId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, dealerId);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public boolean deleteShop(String shopId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM Shop where shopId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, shopId);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public boolean deleteCustomerToShop(String transId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM customer_to_shop where transId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, transId);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public boolean deletePriceListByShop(String shopId) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM price_list where shopId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, shopId);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public boolean deletePriceListByService(String shopId, String seq) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "DELETE FROM price_list where shopId=? AND seq=?;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, shopId);
            statement.setString(2, seq);
            statement.executeQuery();


        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
                return false;
            }
        }
        return true;
    }

    public Customer createCustomer(String firstName, String lastName, String city, String state,
                                   String address, String email, Date birthday) {
        Connection connection = null;
        PreparedStatement statement = null;
        Customer customer = null;
        ResultSet resultSet;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "INSERT INTO customers(firstname,lastname,email,city,state,address,birthday) VALUES (?, ?, ?, ?, ?, ?, ?);";

            statement = connection.prepareStatement(QUERY,Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, email);
            statement.setString(4, city);
            statement.setString(5, state);
            statement.setString(6, address);
            statement.setDate(7, new java.sql.Date(birthday.getTime()));
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();

            customer = new Customer(rs.getString(1), firstName, lastName, city, state, address, email, birthday);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return customer;

    }

    public Dealer createDealer(String firstName, String lastName, String city, String state,
                               String address, String email, Date birthday) {
        Connection connection = null;
        PreparedStatement statement = null;
        Dealer dealer = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "INSERT INTO dealer(firstname,lastname,email,city,state,address,birthday) VALUES (?, ?, ?, ?, ?, ?, ?);";

            statement = connection.prepareStatement(QUERY,Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, email);
            statement.setString(4, city);
            statement.setString(5, state);
            statement.setString(6, address);
            statement.setDate(7, new java.sql.Date(birthday.getTime()));
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();

            dealer = new Dealer(rs.getString(1), firstName, lastName, city, state, address, email, birthday);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return dealer;

    }

    public Transaction createCustToShop(String custId, String shopId, String service, float price) {
        Connection connection = null;
        PreparedStatement statement = null;
        Transaction custToShop = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "INSERT INTO customer_to_shop(custId,shopId,price,service) VALUES (?, ?, ?, ?);";

            statement = connection.prepareStatement(QUERY,Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, custId);
            statement.setString(2, shopId);
            statement.setString(4, service);
            statement.setFloat(5, price);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();

            custToShop = new Transaction(rs.getString(1), custId, shopId, service, price);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return custToShop;

    }

  *//*  public Service createService(String serviceName, String shopId, Float price, int seq) {
        Connection connection = null;
        PreparedStatement statement = null;
        Service service = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "INSERT INTO service(shopId,seq,price,serviceName) VALUES (?, ?, ?, ?);";

            statement = connection.prepareStatement(QUERY);
            statement.setString(4, serviceName);
            statement.setString(1, shopId);
            statement.setFloat(3, price);
            statement.setInt(2, seq);
            statement.executeUpdate();

            service = new Service(serviceName, shopId, price, seq);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return service;

    }*//*

    public Shop createShop(String dealerId, String shopType, String city, String state, String address) {
        Connection connection = null;
        PreparedStatement statement = null;
        Shop shop = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "INSERT INTO Shop(dealerId,city,state,shopType,address) VALUES (?, ?, ?, ?, ?);";

            statement = connection.prepareStatement(QUERY,Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, dealerId);
            statement.setString(4, shopType);
            statement.setString(2, city);
            statement.setString(3, state);
            statement.setString(5, address);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();

            shop = new Shop(rs.getString(1), dealerId, shopType, city, state, address);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return shop;

    }

    public Client getClientForLogin(String user, String psw) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Client client = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "SELECT * FROM client_login where userLogin=? AND passwordLogin=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, user);
            statement.setString(2, psw);
            resultSet = statement.executeQuery();
            if (!resultSet.wasNull()) {
                if (resultSet.getBoolean("isCustomer")) {
                    client = getCustomer(resultSet.getString("custId"));
                } else if (!resultSet.getBoolean("isCustomer")) {
                    client = getDealer(resultSet.getString("custId"));
                } else {
                    return null;
                }
            } else {
                LOGGER.info("Non esistono clienti con questi parametri");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return client;
    }

    public Client createClientForLogin(String user, String psw, String firstName, String lastName, String city, String state,
                                       String address, String email, Date birthday, Boolean isCustomer) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Client client = null;
        String clientId = null;

        try {
            connection = DriverManager.getConnection(getConnectionString());

            if (isCustomer) {
                client = createCustomer(firstName, lastName, city, state, address, email, birthday);
            } else {
                client = createDealer(firstName, lastName, city, state, address, email, birthday);
            }

            if (client == null) {
                return null;
            }

            final String QUERY = "INSERT INTO client_login(custId,userLogin,passwordLogin,isCustomer) VALUE(?,?,?,?)";

            statement = connection.prepareStatement(QUERY);
            if (client instanceof Customer) {
                clientId = ((Customer) client).getCustId();
            } else if (client instanceof Dealer) {
                clientId = ((Dealer) client).getCustId();
            }
            statement.setString(1, clientId);
            statement.setString(2, user);
            statement.setString(3, psw);
            statement.setBoolean(4, isCustomer);
            resultSet = statement.executeQuery();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return client;
    }

    public void updateCustomerToShop(String transId, float price) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "UPDATE customer_to_shop SET price=? WHERE transId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setFloat(1, price);
            statement.setString(2, transId);
            statement.executeUpdate();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

    }

    public void updateShop(String shopId, String shopType) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "UPDATE Shop SET shopType=? WHERE shopId=? ;";

            statement = connection.prepareStatement(QUERY);
            statement.setString(1, shopType);
            statement.setString(2, shopId);
            resultSet = statement.executeQuery();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

    }

    public void updatePriceList(String shopId, int seq, float price) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        try {
            connection = DriverManager.getConnection(getConnectionString());


            final String QUERY = "UPDATE price_list SET price=? WHERE shopId=? AND seq=?;";

            statement = connection.prepareStatement(QUERY);
            statement.setFloat(1, price);
            statement.setString(2, shopId);
            statement.setInt(3, seq);
            resultSet = statement.executeQuery();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

    }*/

}