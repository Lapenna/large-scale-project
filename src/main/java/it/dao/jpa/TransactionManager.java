package it.dao.jpa;

import it.common.model.Transaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class TransactionManager extends PrjManager {

    private static final Logger LOGGER = LogManager.getLogger(TransactionManager.class);

    public Transaction create(Transaction transaction){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(transaction);
            entityManager.getTransaction().commit();
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return transaction;

    }

    public Transaction read(String transId){

        Transaction custToShop = null;

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            custToShop = entityManager.find(Transaction.class, transId);
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return custToShop;

    }

    public List readAll(String id){
        List transactions = null;

        try
        {
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            transactions = entityManager.createQuery("SELECT t FROM Transaction t WHERE t.custId=?1")
                    .setParameter(1, id).getResultList();
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return transactions;
    }

    public List readAllByShop(String id){
        List transactions = null;
        try
        {
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            transactions = entityManager.createQuery("SELECT t FROM Transaction t WHERE t.shopId=?1")
                    .setParameter(1, id).getResultList();
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return transactions;
    }

    public Transaction update(Transaction transaction){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            if (entityManager.find(Transaction.class, transaction.getTransId())==null){
                LOGGER.info("Transaction don't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(transaction);
            entityManager.getTransaction().commit();
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return transaction;
    }


    public boolean delete(Transaction transaction){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(transaction) ? transaction : entityManager.merge(transaction));
            entityManager.getTransaction().commit();

        } catch (Exception ex){
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }

        return true;

    }



}
