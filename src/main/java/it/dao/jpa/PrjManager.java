package it.dao.jpa;

import it.common.chain.IScope;
import it.common.chain.Scope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Properties;

public class PrjManager {
    protected EntityManager entityManager;
    protected static EntityManagerFactory entityManagerFactory;

    private Properties properties = new Properties();

    private static final Logger LOGGER = LogManager.getLogger(PrjManager.class);

    public EntityManagerFactory setup(IScope scope) {
        String dbName = (String)scope.getParameter(Scope.DBNAME);

        String user = (String)scope.getParameter(Scope.USER);
        String pass = (String)scope.getParameter(Scope.PASSWORD);
        String dbConn = (String)scope.getParameter(Scope.DBCONN);
        LOGGER.info("dbName="+dbName);


        /*if (dbName!=null){
            configOverrides.put("javax.persistence.jdbc.url",user);
        }*/
        if (user!=null){
            properties.setProperty("javax.persistence.jdbc.user",user);
            LOGGER.info("user="+user);
        }
        if (pass!=null){
            properties.setProperty("javax.persistence.jdbc.password",pass);
            LOGGER.info("pass="+pass);
        }
        if (dbConn!=null){
            properties.setProperty("javax.persistence.jdbc.url",dbConn);
            LOGGER.info("dbconn="+dbConn);
        }
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory(dbName,properties);
        }catch (Exception e){
            LOGGER.error(e);
        }
        return entityManagerFactory;
    }

    public void exit() {
        entityManager.close();
    }

}
