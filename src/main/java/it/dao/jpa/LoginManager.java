package it.dao.jpa;

import it.common.model.Dealer;
import it.common.model.Login;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginManager extends PrjManager {

    private static final Logger LOGGER = LogManager.getLogger(LoginManager.class);

    public Login create(Login login) {
        LOGGER.info("Creating a new client login");

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(login);
            entityManager.getTransaction().commit();
            LOGGER.info("Client add");

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return login;

    }

    public Login read(String key) {

        Login login = null;

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            login = entityManager.find(Login.class, key);
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return login;

    }

    public Login readbyUser(String userLogin) {

        Login login = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            login = (Login) entityManager.createQuery("SELECT t FROM Login t WHERE t.userLogin = ?1")
                    .setParameter(1, userLogin).getSingleResult();

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return login;

    }

    public Login update(Login login) {

        try {
            entityManager = entityManagerFactory.createEntityManager();
            if (entityManager.find(Dealer.class, login.getCustId()) == null) {
                LOGGER.info("Client don't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(login);
            entityManager.getTransaction().commit();
            LOGGER.info("Update login credential");
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return login;
    }

    public boolean delete(Login login) {

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(login) ? login : entityManager.merge(login));
            entityManager.getTransaction().commit();
            LOGGER.info("Delete complete");
        } catch (Exception ex) {
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }

        return true;

    }
}
