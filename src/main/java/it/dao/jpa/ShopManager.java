package it.dao.jpa;

import java.util.*;
import it.common.model.Shop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShopManager extends PrjManager {

    private static final Logger LOGGER = LogManager.getLogger(ShopManager.class);

    public Shop create (Shop shop){

        try{
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(shop);
            entityManager.getTransaction().commit();

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return shop;
    }

    public Shop read(String shopId){
        Shop shop= null;

        try
        {
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            shop = entityManager.find(Shop.class, shopId);

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return shop;
    }
    public List readAll(){
        List shops = null;
        try
        {
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            shops = entityManager.createQuery("SELECT t FROM Shop t")
                    .getResultList();
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return shops;
    }

    public List readByDealerId(String dealerId){
        List shops = null;
        try
        {
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            shops = entityManager.createQuery("SELECT t FROM Shop t WHERE t.custId=?1")
                    .setParameter(1, dealerId).getResultList();

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return shops;
    }


    public Shop update (Shop shop){

        try{
            entityManager= entityManagerFactory.createEntityManager();
            if (entityManager.find(Shop.class, shop.getShopId())==null){
                LOGGER.info("Shop doesn't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(shop);
            entityManager.getTransaction().commit();

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return shop;
    }

    public boolean delete(Shop shop){
        try{
            entityManager= entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(shop) ? shop : entityManager.merge(shop));
            entityManager.getTransaction().commit();

        } catch (Exception ex) {
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

}
