package it.dao.jpa;

import it.common.interfa.Client;
import it.common.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomerManager extends PrjManager {

    private static final Logger LOGGER = LogManager.getLogger(CustomerManager.class);

    public Client create(Customer client) {

        LOGGER.info("Creating a new Customer");

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(client);
            entityManager.getTransaction().commit();
            LOGGER.info("Customer Add");

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return client;

    }

    public Client read(String custId){

        Customer customer = null;

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            customer = entityManager.find(Customer.class, custId);
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return customer;

    }

    public Client update(Customer client){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            if (entityManager.find(Customer.class,(client).getCustId())==null){
                LOGGER.info("Client don't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(client);
            entityManager.getTransaction().commit();
            LOGGER.info("Update Customer");
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return client;
    }

    public boolean delete(Customer customer){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(customer) ? customer : entityManager.merge(customer));
            entityManager.getTransaction().commit();
            LOGGER.info("Delete complete");
        } catch (Exception ex){
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }

        return true;

    }

}
