package it.dao.jpa;

import it.common.interfa.Client;
import it.common.model.Dealer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DealerManager extends PrjManager {
    private static final Logger LOGGER = LogManager.getLogger(DealerManager.class);

    public Client create(Dealer dealer) {
        LOGGER.info("Creating a new dealer");

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(dealer);
            entityManager.getTransaction().commit();
            LOGGER.info("dealer Add");

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return dealer;

    }

    public Client read(String custId){

        Dealer dealer = null;

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            dealer = entityManager.find(Dealer.class, custId);
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return dealer;

    }

    public Client update(Dealer client){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            if (entityManager.find(Dealer.class,(client).getCustId())==null){
                LOGGER.info("Client don't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(client);
            entityManager.getTransaction().commit();
            LOGGER.info("Update dealer");
        } catch (Exception ex){
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return client;
    }

    public boolean delete(Dealer dealer){

        try{
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(dealer) ? dealer : entityManager.merge(dealer));
            entityManager.getTransaction().commit();
            LOGGER.info("Delete complete");
        } catch (Exception ex){
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }

        return true;

    }

}
