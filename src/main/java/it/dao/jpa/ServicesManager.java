package it.dao.jpa;

import it.common.model.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;


public class ServicesManager extends PrjManager {

    private static final Logger LOGGER = LogManager.getLogger(ServicesManager.class);

    public Service create(Service service) {

        LOGGER.info("Creating a new service");

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(service);
            entityManager.getTransaction().commit();
            LOGGER.info("Service Add");

        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return service;

    }

    public Service read(String serviceId) {

        Service service = new Service();
        service.setServiceId(serviceId);
        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            service = entityManager.find(Service.class, service);
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return service;

    }

    public List readAll(String shopId) {
        List services = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            services = entityManager.createQuery("SELECT t FROM Service t WHERE t.shopId=?1")
                    .setParameter(1, shopId).getResultList();


        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }
        return services;

    }

    public Service update(Service service) {

        try {
            entityManager = entityManagerFactory.createEntityManager();
            if (entityManager.find(Service.class, service.getServiceId()) == null) {
                LOGGER.info("Service don't exist");
                return null;
            }
            entityManager.getTransaction().begin();
            entityManager.merge(service);
            entityManager.getTransaction().commit();
            LOGGER.info("Update Service");
        } catch (Exception ex) {
            LOGGER.error(ex);
        } finally {
            entityManager.close();
        }

        return service;
    }

    public boolean delete(Service service) {

        try {
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.contains(service) ? service : entityManager.merge(service));
            entityManager.getTransaction().commit();
            LOGGER.info("Delete complete");
        } catch (Exception ex) {
            LOGGER.error(ex);
            return false;
        } finally {
            entityManager.close();
        }

        return true;

    }
}
