package it.dao;

import it.common.chain.IScope;
import it.common.interfa.Client;
import it.common.model.*;
import it.dao.jpa.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public final class CRUDGenericManager {
    public final static String CUSTOMER = "customer";
    public final static String DEALER = "dealer";
    public final static String LOGIN = "login";
    public final static String LOGINBYUSER = "loginbyuser";
    public final static String TRANSACTION = "customertoshop";
    public final static String SERVICE = "service";
    public final static String SERVICEALL = "serviceall";
    public final static String SHOP = "shop";
    public final static String SHOPALL = "shopall";
    public final static String SHOPALLBYDEALERID = "shopallbydealerid";
    public final static String TRANSACTIONALL = "customertoshopall";
    public final static String TRASNSACTIONALLBYSHOP = "customertoshopallbyshop";
    private final static PrjManager PRJ_MANAGER = new PrjManager();


    private static final Logger LOGGER = LogManager.getLogger(CRUDGenericManager.class);

    public static void setUpManager(IScope scope){
        PRJ_MANAGER.setup(scope);
    }

    public static void exitManager(){
        PRJ_MANAGER.exit();
    }


    public static Object create(Object object) {
        try {
            if (object instanceof Customer) {
                CustomerManager customerManager = new CustomerManager();
                Client client = customerManager.create((Customer) object);
                return client;
            } else if (object instanceof Dealer) {
                DealerManager dealerManager = new DealerManager();
                Client client = dealerManager.create((Dealer) object);
                return client;
            } else if (object instanceof Transaction) {
                TransactionManager transactionManager = new TransactionManager();
                Transaction transaction = transactionManager.create((Transaction) object);
                return transaction;
            } else if (object instanceof Login) {
                LoginManager loginManager = new LoginManager();
                Login login = loginManager.create((Login) object);
                return login;
            } else if (object instanceof Service) {
                ServicesManager servicesManager = new ServicesManager();
                Service service = servicesManager.create((Service) object);
                return service;
            } else if (object instanceof Shop) {
                ShopManager shopManager = new ShopManager();
                Shop shop = shopManager.create((Shop) object);
                return shop;
            }
        } catch (Exception e) {
            LOGGER.error(e);
            return null;
        }
        return null;

    }

    public static Object update(Object object) {

        try {
            if (object instanceof Customer) {
                CustomerManager customerManager = new CustomerManager();
                Client client = customerManager.update((Customer) object);
                return client;
            } else if (object instanceof Dealer) {
                DealerManager dealerManager = new DealerManager();
                Client client = dealerManager.update((Dealer) object);
                return client;
            } else if (object instanceof Transaction) {
                TransactionManager transactionManager = new TransactionManager();

                Transaction transaction = transactionManager.update((Transaction) object);

                return transaction;
            } else if (object instanceof Login) {
                LoginManager loginManager = new LoginManager();
                Login login = loginManager.update((Login) object);
                return login;
            } else if (object instanceof Service) {
                ServicesManager servicesManager = new ServicesManager();
                Service service = servicesManager.update((Service) object);
                return service;
            } else if (object instanceof Shop) {
                ShopManager shopManager = new ShopManager();
                Shop shop = shopManager.update((Shop) object);
                return shop;
            }
        } catch (Exception e) {
            LOGGER.error(e);
            return null;
        }

        return null;
    }

    public static Object read(String type, String key) {
        Client client;
        ServicesManager serviceManager;
        TransactionManager transactionManager;
        ShopManager shopManager; LoginManager loginManager;
        Login login;
        List transactions;
        List shops;

        try {
            switch (type) {
                case CUSTOMER:
                    CustomerManager customerManager = new CustomerManager();
                    client = customerManager.read(key);
                    return client;
                case DEALER:
                    DealerManager dealerManager = new DealerManager();
                    client = dealerManager.read(key);
                    return client;
                case TRANSACTION:
                    transactionManager = new TransactionManager();

                    Transaction transaction = transactionManager.read(key);

                    return transaction;
                case LOGIN:
                    loginManager = new LoginManager();

                    login = loginManager.read(key);

                    return login;
                case LOGINBYUSER:
                    loginManager = new LoginManager();
                    login = loginManager.readbyUser(key);
                    return login;
                case SERVICE:
                    serviceManager = new ServicesManager();
                    Service service = serviceManager.read(key);
                    return service;
                case SERVICEALL:
                    serviceManager = new ServicesManager();
                    List services = serviceManager.readAll(key);
                    return services;
                case SHOP:
                    shopManager = new ShopManager();

                    Shop shop = shopManager.read(key);

                    return shop;
                case SHOPALL:
                    shopManager = new ShopManager();

                    shops = shopManager.readAll();

                    return shops;
                case SHOPALLBYDEALERID:
                    shopManager = new ShopManager();

                    shops = shopManager.readByDealerId(key);

                    return shops;
                case TRANSACTIONALL:
                    transactionManager = new TransactionManager();
                    transactions = transactionManager.readAll(key);
                    return transactions;
                case TRASNSACTIONALLBYSHOP:
                    transactionManager = new TransactionManager();
                    transactions = transactionManager.readAllByShop(key);
                    return transactions;

            }
        } catch (Exception e) {
            LOGGER.error(e);
            return null;
        }
        return null;
    }

    public static boolean delete(Object object) {
        boolean ok;

        try {
            if (object instanceof Customer) {
                CustomerManager customerManager = new CustomerManager();
                ok = customerManager.delete((Customer)object);
                return ok;
            } else if (object instanceof Dealer) {
                DealerManager dealerManager = new DealerManager();
                ok = dealerManager.delete((Dealer)object);
                return ok;
            }else if (object instanceof Transaction) {
                TransactionManager transactionManager = new TransactionManager();
                ok = transactionManager.delete((Transaction)object);
                return ok;
            } else if (object instanceof Login) {
                LoginManager loginManager = new LoginManager();
                ok = loginManager.delete((Login)object);
                return ok;
            }else if (object instanceof Service) {
                ServicesManager serviceManager = new ServicesManager();
                ok = serviceManager.delete((Service)object);
                return ok;
            } else if (object instanceof Shop) {
                ShopManager shopManager = new ShopManager();
                ok = shopManager.delete((Shop)object);
                return ok;
            }
        } catch (Exception e) {
            LOGGER.error(e);
            return false;
        }

        return false;
    }

}
