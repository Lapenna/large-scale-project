package it.engine.chain;


import it.common.chain.AbsOpChain;
import it.engine.chain.operation.StartOp;

public class EngineOpChain extends AbsOpChain {

    @Override
    protected void populateChain() {

        chain.add(new StartOp());
    }
}
