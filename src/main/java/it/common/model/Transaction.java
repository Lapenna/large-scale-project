package it.common.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "booking")
public class Transaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "transId", unique = true)
    private String transId;

    @Column(name = "custId", nullable = false )
    private String custId;
    @Column(name = "shopId", nullable = false)
    private String shopId;
    @Column
    private String serviceId;

    private float price;

    private Date created;
    private Date updated;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    @ManyToOne
    @JoinColumn(name = "custId", nullable = false,insertable = false,updatable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "shopId", nullable = false,insertable = false,updatable = false)
    private Shop shop;

    @OneToOne
    @JoinColumn(name="serviceId", nullable = false,insertable = false,updatable = false)
    private Service service;

    public Transaction(String transId, String custId, String shopId, String service, float price) {

        this.transId = transId;
        this.custId = custId;
        this.shopId = shopId;
        this.price = price;
    }

    public Transaction() {}

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
