package it.common.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "shop")
public class Shop implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "shopId", unique = true)
    private String shopId;

    @Column(name = "custId", nullable = false)
    private String custId;

    @Column(name = "name", nullable = false)
    private String name;

    private String shopType;
    private String city;
    private String state;
    private String address;

    private Date created;
    private Date updated;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    @OneToMany(mappedBy = "shop")
    private List<Transaction> transactions;

    @OneToMany(mappedBy = "shop")
    private List<Service> service;

    @ManyToOne
    @JoinColumn(name = "custId", nullable = false,updatable = false,insertable = false)
    private Dealer dealer;

    public Shop(String shopId, String custId, String shopType, String city, String state, String address) {
        this.shopId = shopId;
        this.custId = custId;
        this.shopType = shopType;
        this.city = city;
        this.state = state;
        this.address = address;
    }

    public Shop() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Service> getService() {
        return service;
    }

    public void setService(List<Service> service) {
        this.service = service;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String dealerId) { this.custId = dealerId; }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
