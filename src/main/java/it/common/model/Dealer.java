package it.common.model;

import it.common.interfa.Client;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "dealer")
public class Dealer implements Client, Serializable {

    @Id
    @Column(name = "custId", unique = true)
    private String custId;

    private String firstName;
    private String lastName;
    private String city;
    private String state;
    private String address;
    @Column(name = "email", nullable = false)
    private String email;

    private Date created;
    private Date updated;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "custId", referencedColumnName = "custId")
    private Login login;

    @OneToMany(mappedBy = "dealer")
    private List<Shop> shop;

    public Dealer(String custId, String firstName, String lastName, String city, String state, String address, String email) {

        this.custId = custId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
        this.address = address;
        this.email = email;
    }

    public Dealer(String firstName, String lastName, String city, String state, String address, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
        this.address = address;
        this.email = email;
    }

    public Dealer() {
    }


    public List<Shop> getShop() {
        return shop;
    }

    public void setShop(List<Shop> shop) {
        this.shop = shop;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
