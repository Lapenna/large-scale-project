package it.common.chain;

import java.util.HashMap;
import java.util.Map;

public class Scope implements IScope {
    private Map<String, Object> payloads = new HashMap<String, Object>();

    public void addParameter(String key, Object value) {
        payloads.put(key, value);
    }

    public Object getParameter(String key) {
        return payloads.get(key);
    }

    public void modifyParameter(String key,Object value){ payloads.replace(key,value);}

}
