package it.common.chain;

public interface IScope {

    String SHOP = "shop";
    String CUSTOMER = "customer";
    String DEALER = "dealer";
    String CLIENT = "client";
    String SERVICE = "service";
    String TRANSACTION = "transaction";
    String USER = "user";
    String PASSWORD = "password";
    String DBNAME = "DBname";
    String DBCONN = "DBCONN" ;

    void addParameter(String key, Object value);
    Object getParameter(String key);
    void modifyParameter(String key,Object value);

}
