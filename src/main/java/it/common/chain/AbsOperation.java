package it.common.chain;

import it.dao.Dao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public abstract class AbsOperation implements IOperation {

    private static final Logger LOGGER = LogManager.getLogger(AbsOperation.class);

    protected static final String DBCONN = "DBCONN";

    protected Map<String, String> getInitParams(IScope scope) {
        return (Map<String, String>) scope.getParameter("INIT_PARAMS");
    }

    protected String getInitParam(IScope scope, String paramName) {
        Map<String, String> initParams = (Map<String, String>) scope.getParameter("INIT_PARAMS");
        return initParams.get(paramName);
    }

    private String getFileNameAndExtension(String filePath) {
        if (filePath.contains("/")) {
            return filePath.substring(filePath.lastIndexOf("/") + "/".length());
        }
        return filePath.substring(filePath.lastIndexOf("\\") + "\\".length());
    }

    protected String getFileName(String fullSourcePath) {
        //return fullSourcePath.substring(fullSourcePath.lastIndexOf("/") + 1);
        return getFileNameAndExtension(fullSourcePath);
    }

    protected Dao getDao(String dbConn) throws Exception {
        try {
            LOGGER.info("DB Conn [{}]", dbConn);

            //String[] dbConnTokens = parseDbConn(dbConn);
            Dao dao = new Dao();

            // Connection String
            dao.setConnectionString(dbConn);

            // DB User
            //it.dao.setDbUser(dbConnTokens[1]);

            // DB Password
            //it.dao.setDbPwd(dbConnTokens[2]);

            return dao;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new Exception();
        }
    }

    private String[] parseDbConn(String dbConn) {
        return dbConn.split(";");
    }

}
