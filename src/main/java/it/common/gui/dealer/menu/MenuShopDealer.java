package it.common.gui.dealer.menu;

import it.common.chain.IScope;
import it.common.gui.dealer.list.ListTransactionDealer;
import it.common.gui.dealer.list.ListShopDealer;
import it.common.gui.dealer.modify.ModifyInfoShop;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuShopDealer extends JFrame {

    private JPanel pannel;
    private JButton modify;
    private JButton delete;
    private JButton custToShopS;
    private JButton services;
    private JButton back;

    public MenuShopDealer(IScope scope){
        super("SELECT OPERATION");
        pannel = new JPanel();
        modify = new JButton("MODIFY");
        delete = new JButton("DELETE");
        custToShopS = new JButton("TRANSACTIONS");
        services = new JButton("SERVICES");
        back = new JButton("BACK");

        setSize(300,350);
        setLocation(500,280);
        pannel.setLayout (null);

        modify.setBounds(75,40,150,20);
        delete.setBounds(75,75,150,20);
        custToShopS.setBounds(75,110,150,20);
        services.setBounds(75,145,150,20);
        back.setBounds(75,180,150,20);

        pannel.add(modify);
        pannel.add(delete);
        pannel.add(custToShopS);
        pannel.add(services);
        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        modify.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ModifyInfoShop modifyInfoShop = new ModifyInfoShop(scope);
                dispose();

            }
        });

        custToShopS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListTransactionDealer listTransactionDealer = new ListTransactionDealer(scope);
                dispose();

            }
        });

        services.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuServiceShop menuServiceShop = new MenuServiceShop(scope);
                dispose();

            }
        });

        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Shop shop = (Shop)scope.getParameter(IScope.SHOP);
                CRUDGenericManager.delete(shop);

                ListShopDealer listShopDealer = new ListShopDealer(scope);
                dispose();
            }
        });

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListShopDealer listShopDealer = new ListShopDealer(scope);
                dispose();

            }
        });
    }
}
