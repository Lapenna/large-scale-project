package it.common.gui.dealer.menu;

import it.common.chain.IScope;
import it.common.gui.dealer.create.CreateServiceByDealer;
import it.common.gui.dealer.list.ListServiceShop;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuServiceShop extends JFrame {
    private JPanel pannel;
    private JButton create;
    private JButton show;
    private JButton back;

    public MenuServiceShop(IScope scope){
        super("SELECT OPERATION");
        pannel = new JPanel();
        create = new JButton("CREATE");
        show = new JButton("SHOW LIST");
        back = new JButton("BACK");

        setSize(300,200);
        setLocation(500,280);
        pannel.setLayout (null);

        create.setBounds(75,40,150,20);
        show.setBounds(75,75,150,20);
        back.setBounds(75,110,150,20);

        pannel.add(create);
        pannel.add(show);
        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        create.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                CreateServiceByDealer createServiceByDealer = new CreateServiceByDealer(scope);
                dispose();
            }
        });

        show.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListServiceShop listServiceShop = new ListServiceShop(scope);
                dispose();

            }
        });

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuShopDealer menuShopDealer = new MenuShopDealer(scope);
                dispose();

            }
        });
    }

}
