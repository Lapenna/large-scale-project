package it.common.gui.dealer.menu;

import it.common.chain.IScope;
import it.common.gui.Login;
import it.common.gui.dealer.menu.MenuShopsDealer;
import it.common.gui.dealer.modify.ModifyInfoDealer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SelectOperationDealer extends JFrame {
    private JPanel pannel;
    private JButton shop;
    private JButton modify;
    private JButton back;

    public SelectOperationDealer(IScope scope){
        super("SELECT OPERATION");
        pannel = new JPanel();
        shop = new JButton("SHOP");
        modify = new JButton("MODIFY INFO");
        back = new JButton("BACK");

        setSize(300,200);
        setLocation(500,280);
        pannel.setLayout (null);

        shop.setBounds(70,40,150,20);
        modify.setBounds(70,75,150,20);
        back.setBounds(70,110,150,20);

        pannel.add(shop);
        pannel.add(modify);
        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        shop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuShopsDealer menuShopsDealer = new MenuShopsDealer(scope);
                dispose();

            }
        });

        modify.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ModifyInfoDealer modifyInfoDealer = new ModifyInfoDealer(scope);
                dispose();
            }
        });

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Login login = new Login(scope);
                dispose();

            }
        });
    }
}
