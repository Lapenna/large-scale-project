package it.common.gui.dealer.menu;

import it.common.chain.IScope;
import it.common.gui.dealer.list.ListTransactionDealer;
import it.common.gui.dealer.modify.ModifyTransaction;
import it.common.model.Transaction;
import it.dao.CRUDGenericManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuTransaction extends JFrame {

    private JPanel pannel;
    private JButton modify;
    private JButton delete;
    private JButton back;

    public MenuTransaction(IScope scope){
        super("SELECT OPERATION");
        pannel = new JPanel();
        modify = new JButton("MODIFY");
        delete = new JButton("DELETE");
        back = new JButton("BACK");

        setSize(300,200);
        setLocation(500,280);
        pannel.setLayout (null);

        modify.setBounds(110,40,80,20);
        delete.setBounds(110,75,80,20);
        back.setBounds(110,180,80,20);

        pannel.add(modify);
        pannel.add(delete);
        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        modify.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ModifyTransaction modifyInfoShop = new ModifyTransaction(scope);
                dispose();

            }
        });

        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Transaction transaction = (Transaction) scope.getParameter(IScope.TRANSACTION);
                CRUDGenericManager.delete(transaction);

                ListTransactionDealer listServiceShop = new ListTransactionDealer(scope);
                dispose();
            }
        });

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListTransactionDealer listServiceShop = new ListTransactionDealer(scope);
                dispose();

            }
        });
    }

}
