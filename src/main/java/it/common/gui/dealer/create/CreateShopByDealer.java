package it.common.gui.dealer.create;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.gui.dealer.menu.SelectOperationDealer;
import it.common.gui.dealer.menu.MenuShopsDealer;
import it.common.model.Dealer;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateShopByDealer extends JFrame{
    private JButton create;
    private JPanel newShopPanel;
    private JTextField txnameshop;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JButton back;


    private JLabel nameShop;
    private JLabel city;
    private JLabel state;
    private JLabel address;


    private static final Logger LOGGER = LogManager.getLogger(CreateShopByDealer.class);


    public CreateShopByDealer(IScope scope){
        super("CREATE SHOP");

        back = new JButton("BACK");
        create = new JButton("CREATE");
        city = new JLabel("City - ");
        nameShop = new JLabel("Name -");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");


        newShopPanel = new JPanel();
        txcity = new JTextField(15);
        txstate = new JTextField(15);
        txaddress = new JTextField(15);
        txnameshop = new JTextField(15);

        setSize(300,280);
        setLocation(500,280);
        newShopPanel.setLayout (null);

        create.setBounds(75,165,150,20);

        nameShop.setBounds(35,30,80,20);
        txnameshop.setBounds(95,30,150,20);
        city.setBounds(35,60,80,20);
        state.setBounds(35,90,80,20);
        txcity.setBounds(95,60,150,20);
        txstate.setBounds(95,90,150,20);
        address.setBounds(35,120,80,20);
        txaddress.setBounds(95,120,150,20);
        back.setBounds(75,205,150,20);


        newShopPanel.add(nameShop);
        newShopPanel.add(txnameshop);
        newShopPanel.add(create);
        newShopPanel.add(txaddress);
        newShopPanel.add(txcity);
        newShopPanel.add(txstate);
        newShopPanel.add(address);
        newShopPanel.add(city);
        newShopPanel.add(state);
        newShopPanel.add(back);

        getContentPane().add(newShopPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuShopsDealer menuShopsDealer = new MenuShopsDealer(scope);
                dispose();

            }
        });

        create.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = txnameshop.getText();
                    String city = txcity.getText();
                    String state = txstate.getText();
                    String address = txaddress.getText();
                    Shop shop = new Shop();
                    Dealer dealer = (Dealer)scope.getParameter(Scope.DEALER);
                    shop.setName(name);
                    shop.setCity(city);
                    shop.setState(state);
                    shop.setAddress(address);
                    shop.setCustId(dealer.getCustId());

                    CRUDGenericManager.create(shop);

                    SelectOperationDealer selectOperationDealer = new SelectOperationDealer(scope);
                    dispose();

                } catch (Exception d) {
                    LOGGER.error(d);
                }

            }
        });
    }
}
