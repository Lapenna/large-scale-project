package it.common.gui.dealer.create;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.gui.dealer.list.ListServiceShop;
import it.common.gui.dealer.menu.MenuServiceShop;
import it.common.gui.dealer.menu.MenuShopDealer;
import it.common.model.Service;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateServiceByDealer extends JFrame {
    private JButton create;
    private JPanel createServicePanel;
    private JTextField serviceName;
    private JTextField price;
    private JButton back;
    private JLabel serviceNameLable;
    private JLabel priceLable;

    private static final Logger LOGGER = LogManager.getLogger(CreateShopByDealer.class);

    public CreateServiceByDealer(IScope scope) {
        super("CREATE SERVICE");

        back = new JButton("BACK");
        create = new JButton("CREATE");
        serviceNameLable = new JLabel("Name -");
        priceLable = new JLabel("Price -");


        createServicePanel = new JPanel();
        price = new JTextField(15);
        serviceName = new JTextField(15);

        setSize(300, 200);
        setLocation(500, 280);
        createServicePanel.setLayout(null);

        serviceName.setBounds(70, 30, 150, 20);
        serviceNameLable.setBounds(20, 30, 80, 20);
        price.setBounds(70, 60, 150, 20);
        priceLable.setBounds(20, 60, 80, 20);

        create.setBounds(75, 100, 150, 20);
        back.setBounds(75, 130, 150, 20);

        createServicePanel.add(priceLable);
        createServicePanel.add(serviceName);
        createServicePanel.add(create);
        createServicePanel.add(price);
        createServicePanel.add(serviceNameLable);
        createServicePanel.add(back);

        getContentPane().add(createServicePanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MenuServiceShop menuServiceShop = new MenuServiceShop(scope);
                dispose();

            }
        });

        create.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = serviceName.getText();
                    Float floatPrice = Float.parseFloat(price.getText());
                    Service service = new Service();
                    Shop shop = (Shop) scope.getParameter(IScope.SHOP);

                    service.setService(name);
                    service.setPrice(floatPrice);
                    service.setShopId(shop.getShopId());

                    CRUDGenericManager.create(service);

                } catch (Exception d) {
                    LOGGER.error(d);
                }

                MenuServiceShop menuServiceShop = new MenuServiceShop(scope);
                dispose();

            }
        });
    }
}
