package it.common.gui.dealer.list;

import it.common.chain.IScope;
import it.common.gui.dealer.menu.MenuServiceDealer;
import it.common.gui.dealer.menu.MenuServiceShop;
import it.common.gui.dealer.menu.MenuShopDealer;
import it.common.model.Dealer;
import it.common.model.Service;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListServiceShop extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String nameService;
    private float servicePrice;


    public ListServiceShop(IScope scope) {
        super("LIST SERVICE");
        pannel = new JPanel();
        List<Service> services = ((Shop) scope.getParameter(IScope.SHOP)).getService();

        assert services != null;
        int lenght = services.size();
        JButton show[] = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        JLabel price;
        for (Object object : services ){
            Service service = (Service)object;
            nameService = service.getServiceName();
            servicePrice = service.getPrice();
            name = new JLabel(nameService);
            price = new JLabel(String.valueOf(servicePrice));

            show[count] = new JButton("SHOW");
            pannel.add(show[count]);
            pannel.add(name);
            pannel.add(price);

            show[count].setBounds(325,y,150,20);
            name.setBounds(25, y, 125, 20);
            price.setBounds(175, y, 125, 20);
            y = y + 35;

            show[count].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scope.addParameter(IScope.SERVICE, service);
                    MenuServiceDealer menuServiceDealer = new MenuServiceDealer(scope);
                    dispose();
                }
            });
            count++;
        }
        back = new JButton("BACK");

        setSize(500, y+100);
        setLocation(500, 280);
        pannel.setLayout(null);

        back.setBounds(210, y, 80, 20);

        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MenuServiceShop menuServiceShop = new MenuServiceShop(scope);
                dispose();

            }
        });


    }

}
