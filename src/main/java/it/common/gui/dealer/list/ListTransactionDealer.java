package it.common.gui.dealer.list;

import it.common.chain.IScope;
import it.common.gui.dealer.menu.MenuServiceDealer;
import it.common.gui.dealer.menu.MenuTransaction;
import it.common.gui.dealer.menu.MenuShopDealer;
import it.common.model.Service;
import it.common.model.Transaction;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListTransactionDealer extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String nameTransaction;
    private float servicePrice;

    public ListTransactionDealer(IScope scope) {
        super("LIST TRANSACTIONS");
        pannel = new JPanel();
        String key = ((Shop)scope.getParameter(IScope.SHOP)).getShopId();
        List<Transaction> transactions = ((Shop) scope.getParameter(IScope.SHOP)).getTransactions();

        int lenght = transactions.size();
        JButton show[] = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        JLabel price;
        for (Object object : transactions ){
            Transaction transaction = (Transaction)object;
            nameTransaction = transaction.getService().getServiceName();
            name = new JLabel(nameTransaction);
            servicePrice = transaction.getPrice();
            price = new JLabel(String.valueOf(servicePrice));

            show[count] = new JButton("SHOW");
            pannel.add(show[count]);
            pannel.add(name);
            pannel.add(price);

            show[count].setBounds(325, y, 150, 20);
            name.setBounds(25, y, 125, 20);
            price.setBounds(175, y, 125, 20);
            y = y + 35;
            show[count].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scope.addParameter(IScope.TRANSACTION, transaction);
                    MenuServiceDealer menuServiceDealer = new MenuServiceDealer(scope);
                    dispose();
                }
            });
            count++;
        }

        back = new JButton("BACK");

        setSize(500,y+100);
        setLocation(500, 280);
        pannel.setLayout(null);

        back.setBounds(210,y+35,150,20);
        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MenuShopDealer menuShopDealer = new MenuShopDealer(scope);
                dispose();

            }
        });


    }


}
