package it.common.gui.dealer.list;

import it.common.chain.IScope;
import it.common.gui.dealer.menu.MenuServiceDealer;
import it.common.gui.dealer.menu.MenuShopDealer;
import it.common.gui.dealer.menu.SelectOperationDealer;
import it.common.model.Dealer;
import it.common.model.Service;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListShopDealer extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String nameShop;


    public ListShopDealer(IScope scope) {
        super("LIST SHOP");
        pannel = new JPanel();

        List<Shop> shops = ((Dealer) scope.getParameter(IScope.DEALER)).getShop();

        int lenght = shops.size();
        JButton show[] = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        for (Object object : shops ){
            Shop shop = (Shop)object;
            nameShop = shop.getName();
            name = new JLabel(nameShop);

            show[count] = new JButton("SHOW");
            pannel.add(show[count]);
            pannel.add(name);

            show[count].setBounds(160, y, 80, 20);
            name.setBounds(35, y, 130, 20);
            y = y + 35;
            show[count].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scope.addParameter(IScope.SHOP, shop);
                    MenuShopDealer menuShopDealer = new MenuShopDealer(scope);
                    dispose();
                }
            });
            count++;
        }
        back = new JButton("BACK");

        back.setBounds(75, y, 150, 20);

        setSize(300, y+90);
        setLocation(500, 280);
        pannel.setLayout(null);



        pannel.add(back);

        getContentPane().add(pannel);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SelectOperationDealer selectOperationDealer = new SelectOperationDealer(scope);
                dispose();

            }
        });

    }

}
