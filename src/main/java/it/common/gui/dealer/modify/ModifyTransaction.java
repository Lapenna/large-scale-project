package it.common.gui.dealer.modify;

import it.common.chain.IScope;
import it.common.gui.dealer.list.ListTransactionDealer;
import it.common.gui.dealer.menu.MenuTransaction;
import it.common.model.Transaction;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyTransaction extends JFrame {
    private JButton modify;
    private JPanel modifyPanell;
    private JTextField price;
    private JButton back;
    private JLabel priceLable;

    private static final Logger LOGGER = LogManager.getLogger(ModifyInfoShop.class);


    public ModifyTransaction(IScope scope){
        super("MODIFY TRANSACTION");

        Transaction transaction = (Transaction) scope.getParameter(IScope.TRANSACTION);

        back = new JButton("BACK");
        modify = new JButton("MODIFY");

        priceLable = new JLabel("Price -");
        modifyPanell = new JPanel();
        price = new JTextField(String.valueOf(transaction.getPrice()),15);

        setSize(300,300);
        setLocation(500,280);
        modifyPanell.setLayout (null);

        modify.setBounds(70,80,150,20);

        priceLable.setBounds(20,30,80,20);
        price.setBounds(70,30,150,20);
        back.setBounds(70,110,150,20);


        modifyPanell.add(priceLable);
        modifyPanell.add(price);
        modifyPanell.add(modify);
        modifyPanell.add(back);

        getContentPane().add(modifyPanell);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuTransaction menuTransaction = new MenuTransaction(scope);
                dispose();
            }
        });

        modify.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Float newPrice = Float.parseFloat(price.getText());
                    transaction.setPrice(newPrice);

                    Transaction newTransaction = (Transaction) CRUDGenericManager.update(transaction);

                    scope.modifyParameter(IScope.TRANSACTION, newTransaction);

                } catch (Exception d) {
                    LOGGER.error(d);
                }

                ListTransactionDealer listTransactionDealer = new ListTransactionDealer(scope);
                dispose();
            }
        });
    }

}
