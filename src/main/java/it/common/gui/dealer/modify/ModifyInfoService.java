package it.common.gui.dealer.modify;

import it.common.chain.IScope;
import it.common.gui.dealer.list.ListServiceShop;
import it.common.gui.dealer.menu.MenuServiceDealer;
import it.common.model.Service;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyInfoService extends JFrame {
    private JButton modify;
    private JPanel modifyPanell;
    private JTextField price;
    private JButton back;


    private JLabel priceLable;

    private static final Logger LOGGER = LogManager.getLogger(ModifyInfoShop.class);


    public ModifyInfoService(IScope scope){
        super("MODIFY SERVICE");

        Service service = (Service) scope.getParameter(IScope.SERVICE);

        back = new JButton("BACK");
        modify = new JButton("MODIFY");

        priceLable = new JLabel("Price -");
        modifyPanell = new JPanel();
        price = new JTextField(service.getPrice().toString(),15);

        setSize(300,300);
        setLocation(500,280);
        modifyPanell.setLayout (null);

        modify.setBounds(75,80,150,20);

        priceLable.setBounds(20,30,150,20);
        price.setBounds(75,30,150,20);
        back.setBounds(75,125,150,20);


        modifyPanell.add(priceLable);
        modifyPanell.add(price);
        modifyPanell.add(modify);
        modifyPanell.add(back);

        getContentPane().add(modifyPanell);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuServiceDealer menuServiceDealer = new MenuServiceDealer(scope);
                dispose();
            }
        });

        modify.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Float newPrice = Float.parseFloat(price.getText());
                    Service service = (Service) scope.getParameter(IScope.SERVICE);
                    service.setPrice(newPrice);

                    service = (Service) CRUDGenericManager.update(service);
                    scope.modifyParameter(IScope.SERVICE,service);

                } catch (Exception d) {
                    LOGGER.error(d);
                }

                ListServiceShop listServiceShop = new ListServiceShop(scope);
                dispose();
            }
        });
    }

}
