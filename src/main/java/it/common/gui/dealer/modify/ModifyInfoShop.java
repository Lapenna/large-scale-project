package it.common.gui.dealer.modify;

import it.common.chain.IScope;
import it.common.gui.dealer.list.ListShopDealer;
import it.common.gui.dealer.menu.MenuShopDealer;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyInfoShop extends JFrame{
    private JButton modify;
    private JPanel modifyPanell;
    private JTextField txnameshop;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JButton back;


    private JLabel nameShop;
    private JLabel city;
    private JLabel state;
    private JLabel address;

    private static final Logger LOGGER = LogManager.getLogger(ModifyInfoShop.class);


    public ModifyInfoShop(IScope scope){
        super("MODIFY SHOP");

        Shop shop = (Shop) scope.getParameter(IScope.SHOP);

        back = new JButton("BACK");
        modify = new JButton("MODIFY");
        city = new JLabel("City - ");
        nameShop = new JLabel("Name -");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");


        modifyPanell = new JPanel();
        txcity = new JTextField(shop.getCity(),15);
        txstate = new JTextField(shop.getState(),15);
        txaddress = new JTextField(shop.getAddress(),15);
        txnameshop = new JTextField(shop.getName(),15);

        setSize(300,400);
        setLocation(500,280);
        modifyPanell.setLayout (null);

        modify.setBounds(75,280,150,20);

        nameShop.setBounds(20,30,80,20);
        txnameshop.setBounds(70,30,150,20);
        city.setBounds(20,60,80,20);
        state.setBounds(20,90,80,20);
        txcity.setBounds(70,60,150,20);
        txstate.setBounds(70,90,150,20);
        address.setBounds(20,120,80,20);
        txaddress.setBounds(70,120,150,20);
        back.setBounds(75,150,150,20);


        modifyPanell.add(nameShop);
        modifyPanell.add(txnameshop);
        modifyPanell.add(modify);
        modifyPanell.add(txaddress);
        modifyPanell.add(txcity);
        modifyPanell.add(txstate);
        modifyPanell.add(address);
        modifyPanell.add(city);
        modifyPanell.add(state);
        modifyPanell.add(back);

        getContentPane().add(modifyPanell);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                MenuShopDealer menuShopDealer = new MenuShopDealer(scope);
                dispose();
            }
        });

        modify.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = txnameshop.getText();
                    String city = txcity.getText();
                    String state = txstate.getText();
                    String address = txaddress.getText();
                    Shop shop = (Shop) scope.getParameter(IScope.SHOP);
                    shop.setName(name);
                    shop.setCity(city);
                    shop.setState(state);
                    shop.setAddress(address);

                    shop = (Shop)CRUDGenericManager.update(shop);

                    scope.modifyParameter(IScope.SHOP,shop);

                } catch (Exception d) {
                    LOGGER.error(d);
                }
                ListShopDealer listShopDealer = new ListShopDealer(scope);
                dispose();

            }
        });
    }

}
