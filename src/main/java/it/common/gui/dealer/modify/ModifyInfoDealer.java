package it.common.gui.dealer.modify;

import it.common.chain.IScope;
import it.common.gui.dealer.menu.SelectOperationDealer;
import it.common.interfa.Client;
import it.common.model.Dealer;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyInfoDealer extends JFrame {
    private JPanel pannel;
    private JButton modify;
    private JTextField txfirstname;
    private JTextField txlastname;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JTextField txemail;
    private JButton back;
    private JLabel firstName;
    private JLabel lastName;
    private JLabel city;
    private JLabel state;
    private JLabel address;
    private JLabel email;


    private static final Logger LOGGER = LogManager.getLogger(ModifyInfoDealer.class);


    public ModifyInfoDealer(IScope scope){
        super("Registration");

        Client client = (Client)scope.getParameter(IScope.CLIENT);

        back = new JButton("BACK");
        modify = new JButton("MODIFY");
        firstName = new JLabel("Name - ");
        lastName = new JLabel("Surname - ");
        city = new JLabel("City - ");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");
        email = new JLabel("Email - ");


        pannel = new JPanel();
        txfirstname = new JTextField(client.getFirstName(),15);
        txlastname = new JTextField(client.getLastName(),15);
        txcity = new JTextField(client.getCity(),15);
        txstate = new JTextField(client.getState(),15);
        txaddress = new JTextField(client.getAddress(),15);
        txemail = new JTextField(client.getEmail(),15);


        setSize(300,350 );
        setLocation(500,280);
        pannel.setLayout (null);

        modify.setBounds(75,220,150,20);
        back.setBounds(75,260,150,20);
        firstName.setBounds(35,30,80,20);
        lastName.setBounds(35,60,80,20);
        txfirstname.setBounds(105,30,150,20);
        txlastname.setBounds(105,60,150,20);
        city.setBounds(35,90,80,20);
        state.setBounds(35,120,80,20);
        txcity.setBounds(105,90,150,20);
        txstate.setBounds(105,120,150,20);
        address.setBounds(35,150,80,20);
        email.setBounds(35,180,80,20);
        txaddress.setBounds(105,150,150,20);
        txemail.setBounds(105,180,150,20);

        pannel.add(modify);
        pannel.add(txfirstname);
        pannel.add(txlastname);
        pannel.add(txaddress);
        pannel.add(txcity);
        pannel.add(txemail);
        pannel.add(txstate);

        pannel.add(firstName);
        pannel.add(lastName);
        pannel.add(address);
        pannel.add(city);
        pannel.add(email);

        pannel.add(state);
        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                SelectOperationDealer user = new SelectOperationDealer(scope);
                dispose();

            }
        });

        modify.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {

                    String firstName = txfirstname.getText();
                    String lastName = txlastname.getText();
                    String city = txcity.getText();
                    String state = txstate.getText();
                    String address = txaddress.getText();
                    String email = txemail.getText();


                    Client client = (Client) scope.getParameter("client");

                    if (client==null || !(client instanceof Dealer)){
                        LOGGER.error("Cliente vuoto o errato");
                        return;
                    }
                    else {
                        client.setFirstName(firstName);
                        client.setLastName(lastName);
                        client.setCity(city);
                        client.setState(state);
                        client.setState(address);
                        client.setEmail(email);

                        client = (Client)CRUDGenericManager.update(client);
                        scope.modifyParameter("client",client);

                        SelectOperationDealer user = new SelectOperationDealer(scope);
                        dispose();
                    }

                } catch (Exception d) {
                    d.printStackTrace();
                }

            }
        });
    }

}
