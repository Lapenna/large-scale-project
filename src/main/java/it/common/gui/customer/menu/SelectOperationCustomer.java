package it.common.gui.customer.menu;

import it.common.chain.IScope;
import it.common.gui.Login;
import it.common.gui.customer.list.ListShopCustomer;
import it.common.gui.customer.list.ListTransactionCustomer;
import it.common.gui.customer.modify.ModifyInfoCustomer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SelectOperationCustomer extends JFrame {
    private JPanel pannel;
    private JButton shop;
    private JButton modify;
    private JButton transaction;
    private JButton back;

    private static final Logger LOGGER = LogManager.getLogger(SelectOperationCustomer.class);


    public SelectOperationCustomer(IScope scope){
        super("SELECT OPERATION");
        pannel = new JPanel();
        shop = new JButton("SHOP");
        modify = new JButton("MODIFY INFO");
        transaction = new JButton("TRANSACTIONS");
        back = new JButton("BACK");

        setSize(400,250);
        setLocation(500,280);
        pannel.setLayout (null);

        shop.setBounds(110,40,150,20);
        modify.setBounds(110,75,150,20);
        transaction.setBounds(110,110,150,20);
        back.setBounds(110,145,150,20);

        pannel.add(shop);
        pannel.add(modify);
        pannel.add(transaction);
        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        transaction.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListTransactionCustomer listTransactionCustomer = new ListTransactionCustomer(scope);
                dispose();

            }
        });

        shop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListShopCustomer listShopCustomer = new ListShopCustomer(scope);
                dispose();

            }
        });

        modify.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {

                ModifyInfoCustomer modifyInfoCustomer = new ModifyInfoCustomer(scope);
                dispose();

            }
        });

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Login login = new Login(scope);
                dispose();

            }
        });


    }


}
