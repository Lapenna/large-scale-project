package it.common.gui.customer.list;
import it.common.gui.customer.list.ListServiceCustomer;
import it.common.gui.customer.menu.SelectOperationCustomer;
import it.common.model.Shop;
import it.dao.CRUDGenericManager;
import it.common.chain.IScope;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

public class ListShopCustomer extends JFrame{
    private JPanel pannel;
    private JButton back;
    private String nameType;
    private String nameShop;



    public ListShopCustomer(IScope scope){
        super("LIST SHOP");
        pannel = new JPanel();

        List shops = (List)CRUDGenericManager.read(CRUDGenericManager.SHOPALL,null);

        int lenght = shops.size();
        JButton show[] = new JButton[lenght];
        int count = 0;
        int y=40;
        JLabel name;
        for (Object object : shops ){
            Shop shop = (Shop)object;
            nameShop = shop.getName();
            nameType = shop.getShopType();
            name = new JLabel(nameShop);

            show[count] = new JButton("SHOW");
            pannel.add(show[count]);
            pannel.add(name);

            show[count].setBounds(160, y, 80, 20);
            name.setBounds(35, y, 150, 20);
            y = y + 35;
            show[count].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    scope.addParameter(IScope.SHOP, shop);
                    ListServiceCustomer listServiceCustomer = new ListServiceCustomer(scope);
                    dispose();
                }
            });
            count++;
        }

        back = new JButton("BACK");

        setSize(300,y+130);
        setLocation(500,280);
        pannel.setLayout (null);

        back.setBounds(75,y+35,150,20);

        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);


        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                SelectOperationCustomer selectOperationCustomer = new SelectOperationCustomer(scope);
                dispose();

            }
        });


    }
}
