package it.common.gui.customer.list;

import it.common.chain.IScope;
import it.common.gui.customer.menu.SelectOperationCustomer;
import it.common.model.Customer;
import it.common.model.Service;
import it.common.model.Shop;
import it.common.model.Transaction;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class ListServiceCustomer extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String nameService;
    private float servicePrice;

    private static final Logger LOGGER = LogManager.getLogger(CRUDGenericManager.class);


    public ListServiceCustomer(IScope scope){

        super("LIST SHOP");
        pannel = new JPanel();
        List<Service> services = ((Shop) scope.getParameter(IScope.SHOP)).getService();

        int lenght = services.size();
        JButton book[] = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        JLabel price;
        for (Object object : services ){

            Service service = (Service)object;
            nameService = service.getServiceName();
            servicePrice = service.getPrice();
            name = new JLabel(nameService);
            price = new JLabel(String.valueOf(servicePrice));

            book[count] = new JButton("BOOK");
            pannel.add(book[count]);
            pannel.add(name);
            pannel.add(price);

            book[count].setBounds(325,y,150,20);
            name.setBounds(25, y, 125, 20);
            price.setBounds(175, y, 125, 20);
            y = y + 35;

            book[count].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e) {
                    scope.addParameter(IScope.SERVICE, service);
                    createTransaction(scope);
                    SelectOperationCustomer selectOperationCustomer = new SelectOperationCustomer(scope);
                    dispose();
                }
            });
            count++;
        }


        back = new JButton("BACK");

        setSize(500,y+100);
        setLocation(500,280);
        pannel.setLayout (null);

        back.setBounds(210,y+35,150,20);

        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                ListShopCustomer listShopCustomer = new ListShopCustomer(scope);
                dispose();

            }
        });
    }

    private void createTransaction(IScope scope) {
        try {

            Customer customer = (Customer) scope.getParameter(IScope.CLIENT);
            String custId= customer.getCustId();
            Service service = (Service) scope.getParameter(IScope.SERVICE);
            String serviceId= service.getServiceId();
            Shop shop = (Shop) scope.getParameter(IScope.SHOP);
            String shopId= shop.getShopId();
            Transaction transaction = new Transaction();
            transaction.setServiceId(serviceId);
            transaction.setCustId(custId);
            transaction.setPrice(((Service) scope.getParameter(IScope.SERVICE)).getPrice());
            transaction.setShopId(shopId);
            CRUDGenericManager.create(transaction);

        } catch (Exception d) {
            LOGGER.error(d);
        }
    }
}
