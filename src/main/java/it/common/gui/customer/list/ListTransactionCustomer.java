package it.common.gui.customer.list;

import it.common.chain.IScope;
import it.common.gui.customer.menu.SelectOperationCustomer;
import it.common.interfa.Client;
import it.common.model.Customer;
import it.common.model.Service;
import it.common.model.Transaction;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ListTransactionCustomer extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String nameTransaction;
    private float servicePrice;

    private static final Logger LOGGER = LogManager.getLogger(ListTransactionCustomer.class);

    public ListTransactionCustomer(IScope scope){

        super("LIST TRANSACTIONS");
        pannel = new JPanel();

        List<Transaction> transactions = ((Customer) scope.getParameter(IScope.CUSTOMER)).getTransactions();

        int lenght = transactions.size();
        JButton delete[] = new JButton[lenght];

        int count = 0;
        int y = 30;
        JLabel name;
        JLabel price;
        for (Object object : transactions){
            Transaction transaction = (Transaction)object;
            nameTransaction = transaction.getService().getServiceName();
            servicePrice = transaction.getPrice();
            name = new JLabel(nameTransaction);
            price = new JLabel(String.valueOf(servicePrice));

            delete[count] = new JButton("DELETE");
            pannel.add(delete[count]);
            pannel.add(name);
            pannel.add(price);

            delete[count].setBounds(325, y, 150, 20);
            name.setBounds(25, y, 125, 20);
            price.setBounds(175, y, 125, 20);
            y = y + 35;

            delete[count].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    CRUDGenericManager.delete(transaction);

                    SelectOperationCustomer selectOperationCustomer = new SelectOperationCustomer(scope);
                    dispose();
                }
            });
            count++;
        }


        back = new JButton("BACK");
        back.setBounds(210,y+35,150,20);
        pannel.add(back);

        setSize(500,y+100);
        setLocation(500,280);
        pannel.setLayout (null);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                SelectOperationCustomer selectOperationCustomer= new SelectOperationCustomer(scope);
                dispose();

            }
        });

    }

}
