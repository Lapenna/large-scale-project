package it.common.gui;
import it.common.chain.IScope;
import it.common.interfa.Client;
import it.common.model.Customer;
import it.common.model.Dealer;
import it.dao.CRUDGenericManager;
import it.dao.Dao;
import it.dao.jpa.CustomerManager;
import it.dao.jpa.DealerManager;
import it.dao.jpa.LoginManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.util.Date;
import java.util.Map;

public class NewUser extends JFrame {
    private JButton create;
    private JPanel newUserPanel;
    private JTextField txuserer;
    private JTextField passer;
    private JTextField txfirstname;
    private JTextField txlastname;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JTextField txemail;
    private JButton back;
 /*   private DatePicker datePicker;*/
    private JCheckBox checkCustomer;

    private JLabel username;
    private JLabel password;
    private JLabel firstName;
    private JLabel lastName;
    private JLabel city;
    private JLabel state;
    private JLabel address;
    private JLabel email;
    private JLabel birthday;
    private JLabel isCustomer;


    private IScope scope;

    private static final Logger LOGGER = LogManager.getLogger(NewUser.class);


    public NewUser(IScope scope){
        super("Registration");

        back = new JButton("BACK");
        create = new JButton("SIGN UP");
        username = new JLabel("User - ");
        password = new JLabel("Pass - ");
        firstName = new JLabel("Name - ");
        lastName = new JLabel("Surname - ");
        city = new JLabel("City - ");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");
        email = new JLabel("Email - ");
        birthday = new JLabel("Birthday - ");
        isCustomer = new JLabel("Are you a Customer? - ");

        newUserPanel = new JPanel();
        txuserer = new JTextField(15);
        txfirstname = new JTextField(15);
        passer = new JPasswordField(15);
        txlastname = new JTextField(15);
        txcity = new JTextField(15);
        txstate = new JTextField(15);
        txaddress = new JTextField(15);
        txemail = new JTextField(15);
        /*datePicker = new DatePicker();*/
        checkCustomer = new JCheckBox();


        setSize(300,420);
        setLocation(500,280);
        newUserPanel.setLayout (null);


        txuserer.setBounds(90,30,150,20);
        passer.setBounds(90,65,150,20);
        create.setBounds(70,340,150,20);
        username.setBounds(20,28,80,20);
        password.setBounds(20,63,80,20);
        firstName.setBounds(20,93,80,20);
        lastName.setBounds(20,130,80,20);

        txfirstname.setBounds(90,95,150,20);
        txlastname.setBounds(90,132,150,20);
        city.setBounds(20,160,80,20);
        state.setBounds(20,190,80,20);
        txcity.setBounds(90,162,150,20);
        txstate.setBounds(90,192,150,20);
        address.setBounds(20,220,80,20);
        email.setBounds(20,250,80,20);
        txaddress.setBounds(90,222,150,20);
        txemail.setBounds(90,252,150,20);
       /* birthday.setBounds(20,280,80,20);*/
        isCustomer.setBounds(20,280,200,20);

        /*datePicker.setLayoutX(70);
        datePicker.setLayoutY(282);*/
        checkCustomer.setBounds(222,280,150,20);
        back.setBounds(70,310,150,20);

        newUserPanel.add(create);
        newUserPanel.add(txuserer);
        newUserPanel.add(passer);
        newUserPanel.add(txfirstname);
        newUserPanel.add(txlastname);
        newUserPanel.add(txaddress);
        newUserPanel.add(txcity);
        newUserPanel.add(txemail);
        newUserPanel.add(checkCustomer);
        newUserPanel.add(txstate);
        newUserPanel.add(username);
        newUserPanel.add(password);

        newUserPanel.add(firstName);
        newUserPanel.add(lastName);
        newUserPanel.add(address);
        newUserPanel.add(city);
        newUserPanel.add(email);
        newUserPanel.add(isCustomer);
        newUserPanel.add(state);
        newUserPanel.add(back);

        getContentPane().add(newUserPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Login user = new Login(scope);
                dispose();

            }
        });

        create.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {

                    String punamer = txuserer.getText();
                    String ppaswder = passer.getText();

                    String firstName = txfirstname.getText();
                    String lastName = txlastname.getText();
                    String city = txcity.getText();
                    String state = txstate.getText();
                    String address = txaddress.getText();
                    String email = txemail.getText();
                    boolean isCust = checkCustomer.isSelected();


                    Client client = null;

              /*      while (scan.hasNext()) {
                        usertxter = scan.nextLine();
                        passtxter = scan.nextLine();
                    }*/

                    if(clientExist(punamer)) {
                        JOptionPane.showMessageDialog(null,"Username is already in use");
                        txuserer.setText("");
                        passer.setText("");
                        txuserer.requestFocus();

                    }
                    else if(punamer.equals("") && ppaswder.equals("")){
                        JOptionPane.showMessageDialog(null,"Please insert Username and Password");
                    }
                    else {
                     /*   filewrite.write(punamer+"\r\n" +ppaswder+ "\r\n");
                        filewrite.close();*/
                        if (createClient(punamer,ppaswder,firstName,lastName,city,state,address,email,isCust,scope)){
                            JOptionPane.showMessageDialog(null,"Account has been created.");
                            dispose();
                            Login log = new Login(scope);
                        }
                        else
                            JOptionPane.showMessageDialog(null,"Account has been not created.");

                    }
                } catch (Exception d) {
                    d.printStackTrace();
                }

            }
        });
    }

    private boolean clientExist(String user) throws Exception {
        it.common.model.Login login = (it.common.model.Login)CRUDGenericManager.read(CRUDGenericManager.LOGIN,user);
        if (login!=null){
            return true;
        }
        return false;
    }

    private boolean createClient(String user,String psw,String firstName, String lastName,String city,
                              String state,String address, String email,Boolean isCustomer,IScope scope){
        Client client ;
        client=createClientForLogin(user,psw,firstName,lastName,city,state,address,email,isCustomer,scope);
        if (client==null){
            return false;
        }
        return true;
    }

    public Client createClientForLogin(String user, String psw, String firstName, String lastName, String city, String state,
                                       String address, String email, Boolean isCustomer,IScope scope) {

        Client client ;
        it.common.model.Login login;

        login = new it.common.model.Login();
        login.setUserLogin(user);
        login.setPasswordLogin(psw);
        login.setCustomer(isCustomer);
        login = (it.common.model.Login)CRUDGenericManager.create(login);

        if (isCustomer) {
            Customer customer = new Customer(login.getCustId(),firstName,lastName,city,state,address,email);
            client = (Customer)CRUDGenericManager.create(customer);

        } else {
            Dealer dealer = new Dealer(login.getCustId(),firstName,lastName,city,state,address,email);
            client = (Client)CRUDGenericManager.create(dealer);
        }

        if (client == null) {
            return null;
        }

        scope.addParameter("login",login);

        return client;
    }

}
