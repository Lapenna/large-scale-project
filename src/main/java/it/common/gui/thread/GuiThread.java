package it.common.gui.thread;

import it.common.chain.IScope;
import it.common.gui.Login;

public class GuiThread extends Thread {
    private IScope scope;

    public GuiThread(IScope scope) {

        this.scope = scope;
    }

    public void run(){
        Login login = new Login(scope);
    }
}
