package it.common.gui;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.gui.customer.menu.SelectOperationCustomer;
import it.common.gui.dealer.menu.SelectOperationDealer;
import it.common.interfa.Client;
import it.common.model.Customer;
import it.common.model.Dealer;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.*;

public class Login extends JFrame {
    private JButton blogin;
    private JPanel loginpanel;
    private JTextField txuser;
    private JTextField pass;
    private JButton newUSer;
    private JLabel username;
    private JLabel password;

    private static final Logger LOGGER = LogManager.getLogger(Login.class);

    public Login(IScope scope){
        super("Login Autentification");

        blogin = new JButton("LOGIN");
        loginpanel = new JPanel();
        txuser = new JTextField(15);
        pass = new JPasswordField(15);
        newUSer = new JButton("SIGN UP");
        username = new JLabel("User - ");
        password = new JLabel("Pass - ");

        setSize(300,230);
        setLocation(500,280);
        loginpanel.setLayout (null);


        txuser.setBounds(70,30,150,20);
        pass.setBounds(70,65,150,20);
        blogin.setBounds(110,100,80,20);
        newUSer.setBounds(110,135,80,20);
        username.setBounds(20,28,80,20);
        password.setBounds(20,63,80,20);

        loginpanel.add(blogin);
        loginpanel.add(txuser);
        loginpanel.add(pass);
        loginpanel.add(newUSer);
        loginpanel.add(username);
        loginpanel.add(password);

        getContentPane().add(loginpanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        blogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {

                    String usertxt = " ";
                    String passtxt = " ";
                    String puname = txuser.getText();
                    String ppaswd = pass.getText();
                    Client client = null;

                    if(!puname.equals(usertxt) && !ppaswd.equals(passtxt)) {
                        client = getClient(scope,puname,ppaswd);
                        if(client!=null) {
                            scope.addParameter(IScope.CLIENT,client);
                            if (client instanceof Customer){
                                SelectOperationCustomer selectOperationCustomer = new SelectOperationCustomer(scope);
                                dispose();
                            }
                            if (client instanceof Dealer){
                                SelectOperationDealer selectOperationDealer = new SelectOperationDealer(scope);
                                dispose();
                            }

                        }
                        else{
                            JOptionPane.showMessageDialog(null,"Wrong Username / Password");
                            txuser.setText("");
                            pass.setText("");
                            txuser.requestFocus();
                        }
                    }
                    else if(puname.equals("") && ppaswd.equals("")){
                        JOptionPane.showMessageDialog(null,"Please insert Username and Password");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"Wrong Username / Password");
                        txuser.setText("");
                        pass.setText("");
                        txuser.requestFocus();
                    }
                } catch (Exception d) {
                    d.printStackTrace();
                }

            }
        });

        newUSer.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                NewUser user = new NewUser(scope);
                dispose();

            }
        });
    }

    private Client getClient(IScope scope,String user,String psw) throws Exception {
        Client client = null;
        try{
            it.common.model.Login login=(it.common.model.Login)CRUDGenericManager.read(CRUDGenericManager.LOGINBYUSER,user);
            if (login.getPasswordLogin().compareTo(psw)==0){
                if (login.isCustomer()) {
                    client = (Client) CRUDGenericManager.read(CRUDGenericManager.CUSTOMER,login.getCustId());
                    scope.addParameter(Scope.CUSTOMER,client);
                } else {
                    client = (Client) CRUDGenericManager.read(CRUDGenericManager.DEALER,login.getCustId());
                    scope.addParameter(Scope.DEALER,client);
                }
            }

        }catch (Exception e){
            LOGGER.error(e);
            return null;
        }

        return client;
    }
}