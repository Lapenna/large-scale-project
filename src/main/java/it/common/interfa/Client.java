package it.common.interfa;

public interface Client {

    public String getCustId();
    public void setCustId(String custId);
    public String getFirstName();
    public void setFirstName(String firstName);
    public String getLastName();
    public void setLastName(String lastName);
    public String getCity();
    public void setCity(String city);
    public String getState();
    public void setState(String state);
    public String getAddress();
    public void setAddress(String address);
    public String getEmail();
    public void setEmail(String email);
}
