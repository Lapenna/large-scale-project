:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Procedura per esame DB
:: [OC], 11/10/2019
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CLS

SET CP=./LargeScaleProject.jar
SET MAIN=it.engine.cli.Runner

SET DBNAME=LargeDB
SET USER=root
SET PASS=pass
SET DBCONN=jdbc:mysql://localhost:3306/LargeDB?serverTimezone=UTC

@ECHO Exec Application
java -Drelate.log.file=./../log -cp %CP% %MAIN% --dbname %DBNAME% --user %USER% --password %PASS% --dbconn %DBCONN%

exit /b 0
