#!/bin/sh
echo 'Procedura per esame DB'

CP="./LargeScaleProject.jar"
MAIN="it.engine.cli.Runner"

DBNAME="LargeDB"
USER="root"
PASS="pass"
DBCONN="jdbc:mysql://localhost:3306/LargeDB?serverTimezone=UTC"

java -Drelate.log.file=./../log -cp $CP $MAIN --dbname $DBNAME --user $USER --password $PASS --dbconn $DBCONN
